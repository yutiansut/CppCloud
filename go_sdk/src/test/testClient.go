package main

import (
	"fmt"
	"protocol"
)

/**
 * 测试TCPClient类功能
 */

func main1() {
	appAttr := make(map[string]interface{})
	appAttr["svrid"] = 205
	appAttr["svrname"] = "goSvr"
	capp := protocol.CreateCppCloudApp("cppcloud.cn:4800", appAttr, 3)
	if nil == capp || 0 != capp.Start() {
		fmt.Println("client start fail")
		return
	}

	// 分布式配置测试
	cnf,_ := capp.ConfInstance("test1.json", "app1.json")
	if nil == cnf {
		return
	}

	//confVal := cnf.QueryValue("test1.json/author", nil)
	//fmt.Println("confVal:", confVal)
	fmt.Printf("配置查询，查询key格式：filename/key1/key2\n")

	for {
		fmt.Println("input querykey for query:")
		var input string
		fmt.Scanln(&input)
		if "quit" == input {
			break
		} else if len(input) < 3 {
			continue
		}
		
		confVal := cnf.QueryValue(input, nil)
		fmt.Printf("query config [%s]: %v\n", input, confVal)
	}

	fmt.Println("to do exit handle")
	capp.Shutdown()
	capp.Join()

	fmt.Println("main end")
}
