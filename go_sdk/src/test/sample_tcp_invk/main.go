package main

import (
	"cppcloud"
	"cppcloud/consumer"
	"fmt"
)

func main() {
	appAttr := make(map[string]interface{})
	appAttr["svrid"] = 210
	appAttr["svrname"] = "go-tcp-ivk"
	capp := cppcloud.CreateCppCloudApp("cppcloud.cn:4800", appAttr, 3) // 创建sdk主对象
	capp.Start()

	conMgr := consumer.CreateConsumerManage(capp, "go-tcp-prvd", "TestPrvd") // 创建服务消费者管理对象
	msgSend := "hello cppcloud"
	for {
		// 发起服务调用 （消费'go-tcp-prvd'的服务)
		rspmsg, err := conMgr.RequestTCP("go-tcp-prvd", cppcloud.CMD_TCP_SVR_REQ, msgSend)
		fmt.Println("请求：", msgSend)
		fmt.Println("响应：", rspmsg, err)
		if nil != err {
			break
		}

		fmt.Scanln(&msgSend)
		if "quit" == msgSend {
			break
		}
	}

	fmt.Println("program quit")
	capp.Shutdown()
	capp.Join()
}
