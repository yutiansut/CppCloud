package main

import (
	"cppcloud"
	"fmt"
)

// 使用go sdk创建分布式服务提供者示例

func main() {
	appAttr := make(map[string]interface{})
	appAttr["svrid"] = 208
	appAttr["svrname"] = "go-tcp-prvd"
	capp := cppcloud.CreateCppCloudApp("cppcloud.cn:4800", appAttr, 3) // 创建sdk主对象
	capp.Start()

	prvd := cppcloud.CreateTCPProvider(capp, "", 2044)
	prvd.RegisteTCPCallback(cppcloud.CMD_TCP_SVR_REQ, tcpCallBack)

	prvd.Start()
	prvd.Join()
}

// 服务提供回调
func tcpCallBack(cmdid uint16, msg string, helper *cppcloud.ResponseHelper) {
	fmt.Println("TCPPRVD| msg=recv invoke| body=" + msg)

	// 发送响应数据
	helper.SendMessage("hi,welcome!")
}
