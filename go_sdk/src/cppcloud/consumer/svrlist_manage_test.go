package consumer

import (
	"cppcloud"
	"fmt"
	"testing"
	"time"
)

func TestRamdom(t *testing.T) {
	appAttr := make(map[string]interface{})
	appAttr["svrid"] = 204
	appAttr["svrname"] = "go-tcp-ivk"
	capp := cppcloud.CreateCppCloudApp("cppcloud.cn:4800", appAttr, 3)
	capp.Start()

	conMgr := CreateConsumerManage(capp, "go-tcp-prvd", "httpApp1")
	rspmsg, err := conMgr.RequestTCP("go-tcp-prvd", cppcloud.CMD_TCP_SVR_REQ, "hello cppcloud")
	fmt.Println(rspmsg, err)
	time.Sleep(time.Second * 10)

	capp.Shutdown()
	capp.Join()
}
